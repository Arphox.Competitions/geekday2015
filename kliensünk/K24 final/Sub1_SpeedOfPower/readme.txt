A k�vetkez� �llom�nyok els� sor�ban a bemenet, m�g a tov�bbi sorokban a script konzolra �rt kimenete l�that�.
A feladat elk�sz�t�se sor�n az els� sor �rt�keit a felhaszn�l� adja meg, m�g a tov�bbi sorokat a konzolon kell megjelen�teni.

The first line contains the input, the other lines contain the console output.
During the execution: the first line is specified by the user, the rest should be displayed on the screen.