The UDP system has changed! (hence, the ips.txt has changed as well...)

The network switches seem to block the multicast packages and thus it does not work. Instead, we are changing to unicast UDP. This means two changes:

1) The format of the LOGIN command is different:
/LOGIN USER PASS UDP_IP UDP_PORT
UDP_IP should be the IP address of YOUR computer
UDP_PORT should be the port number where you have a working UDP listener!

2) In your UDP listener, instead of binding to the multicast ip (224.5.6.7:9999), you should bind your UDP listener to UDP_IP : UDP_PORT . Also, you do not need SocketOptionName.AddMembership / MulticastOption calls in your code.

The data format of the UDP packets is the same. After you connect, the server will start sending the UDP packets to your UDP_IP : UDP_PORT