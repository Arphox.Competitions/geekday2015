﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using karesz;

namespace ConsoleApplication1
{
    public static class GameLogic
    {
        static float tdX = -1;
        static float tdY = -1;
        static Random rnd = new Random();
        static Stack<MapItem> toProcess = new Stack<MapItem>();
        static MapItem[,] mapItems = new MapItem[XMLFileProcessor.Map.GetLength(0), XMLFileProcessor.Map.GetLength(1)];
        public static int Strategy
        {
            get { return 1; }
        }
        public static bool IsDeadEnd(int PosX, int PosY)
        {
            string idenemszabad = "VWXYT";
            int cnt = 0;
            if (idenemszabad.Contains(XMLFileProcessor.Map[PosX + 1, PosY])) cnt++;  //Jobbra?
            if (idenemszabad.Contains(XMLFileProcessor.Map[PosX - 1, PosY])) cnt++; //Balra?
            if (idenemszabad.Contains(XMLFileProcessor.Map[PosX, PosY - 1])) cnt++;  //Felfele?
            if (idenemszabad.Contains(XMLFileProcessor.Map[PosX, PosY + 1])) cnt++;   //Balra?
            if (cnt > 2)
            {
                return true;
            }
            return false;
        }
        static MapItem FindFreeSpot()
        {
            for (int i = mapItems.GetLength(0) / 2; i < mapItems.GetLength(0); i++)
            {
                for (int j = mapItems.GetLength(1) / 2; j < mapItems.GetLength(1); j++)
                {
                    if (mapItems[i, j].Type == 'Z')
                    {
                        return mapItems[i, j];
                    }
                }
            }
            return null;
        }
        public static void CalculateMoves()
        {
            for (int i = 0; i < mapItems.GetLength(0); i++)
            {
                for (int j = 0; j < mapItems.GetLength(1); j++)
                {
                    char c = XMLFileProcessor.Map[i, j];
                    if ((c <= '9' && c >= '0') || (c <= 'H' && c >= 'A') || (c <= 'S' && c >= 'O') || c == 'U')
                    {
                        c = 'Z';
                    }
                    mapItems[i, j] = new MapItem(i, j, c);
                }
            }
            MapItem destination = FindFreeSpot();
            if (Strategy == 0) //1v1 stratégia
            {

            }
            else //free for all stratégia
            {

                try
                {
                    if (XMLFileProcessor.MyPlayer.ArmorStepsRemaining > 0)
                    {
                        Commands.Stop();
                        Commands.Bomb();
                    }
                    #region asd
                    else
                    {
                        int minindex = ClosestPowerUp();
                        if (minindex >= 0)
                        {
                            Powerup closest = XMLFileProcessor.Powerups[minindex];
                            if (!Danger(closest.PosX, closest.PosY))
                            {
                                PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, closest.PosX, closest.PosY);
                            }
                            else
                            {
                                MapItem safeSpot = SearchForSafeSpot();
                                if (safeSpot != null)
                                {
                                    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, safeSpot.X, safeSpot.Y);
                                }
                            }
                        }
                        else
                        {
                            if (!Danger(destination.X, destination.Y))
                            {
                                PathFinder((int)XMLFileProcessor.MyPlayer.PosX, (int)XMLFileProcessor.MyPlayer.PosY, destination.X, destination.Y);
                            }
                            //if (mapItems[9,9].Type == 'Z' && !Danger(9, 9))
                            //{
                            //    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, 9, 9);
                            //    return;
                            //}
                            //if (mapItems[10, 10].Type == 'Z' && !Danger(10, 10))
                            //{
                            //    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, 10, 10);
                            //    return;
                            //}
                            //if (mapItems[11, 11].Type == 'Z' && !Danger(11, 11))
                            //{
                            //    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, 11, 11);
                            //    return;
                            //}
                            //if (mapItems[12, 5].Type == 'Z' && !Danger(12, 5))
                            //{
                            //    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, 12, 5);
                            //    return;
                            //}
                            //if (mapItems[14, 10].Type == 'Z' && !Danger(14, 10))
                            //{
                            //    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, 14, 10);
                            //    return;
                            //}
                            //if (mapItems[3, 4].Type == 'Z' && !Danger(3, 4))
                            //{
                            //    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, 3, 4);
                            //    return;
                            //}
                            //if (mapItems[10, 2].Type == 'Z' && !Danger(10, 2))
                            //{
                            //    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, 10, 2);
                            //    return;
                            //}
                            //if (mapItems[12, 12].Type == 'Z' && !Danger(12, 12))
                            //{
                            //    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, 12, 12);
                            //    return;
                            //}
                            //if (mapItems[1, 5].Type == 'Z' && !Danger(1, 5))
                            //{
                            //    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, 1, 5);
                            //    return;
                            //}
                            //if (mapItems[6, 7].Type == 'Z' && !Danger(6, 7))
                            //{
                            //    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, 6, 7);
                            //    return;
                            //}
                            //int newX = rnd.Next(0, 5);
                            //if (newX == 0)
                            //{
                            //    for (int i = 0; i < 5; i++)
                            //    {
                            //        if ((int)XMLFileProcessor.MyPlayer.PosX + i < mapItems.GetLength(0) && !Danger((int)XMLFileProcessor.MyPlayer.PosX + i, (int)XMLFileProcessor.MyPlayer.PosY))
                            //        {
                            //            PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, XMLFileProcessor.MyPlayer.PosX + i, XMLFileProcessor.MyPlayer.PosY);
                            //        }
                            //    }
                            //}
                            //else if (newX == 1)
                            //{
                            //    for (int i = 0; i < 5; i++)
                            //    {
                            //        if ((int)XMLFileProcessor.MyPlayer.PosX - i >= 0 && !Danger((int)XMLFileProcessor.MyPlayer.PosX - i, (int)XMLFileProcessor.MyPlayer.PosY))
                            //        {
                            //            PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, XMLFileProcessor.MyPlayer.PosX - i, XMLFileProcessor.MyPlayer.PosY);
                            //        }
                            //    }
                            //}
                            //else if (newX == 2)
                            //{
                            //    for (int i = 0; i < 5; i++)
                            //    {
                            //        if ((int)XMLFileProcessor.MyPlayer.PosY - i >= 0 && !Danger((int)XMLFileProcessor.MyPlayer.PosX, (int)XMLFileProcessor.MyPlayer.PosY - i))
                            //        {
                            //            PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY - i);
                            //        }
                            //    }
                            //}
                            //else if (newX == 3)
                            //{
                            //    for (int i = 0; i < 5; i++)
                            //    {
                            //        if ((int)XMLFileProcessor.MyPlayer.PosY + i < mapItems.GetLength(1) && !Danger((int)XMLFileProcessor.MyPlayer.PosX, (int)XMLFileProcessor.MyPlayer.PosY + i))
                            //        {
                            //            PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY + i);
                            //        }
                            //    }
                            //}
                            else
                            {
                                MapItem safeSpot = SearchForSafeSpot();
                                if (safeSpot != null)
                                {
                                    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, safeSpot.X, safeSpot.Y);
                                }
                            }
                        }
                    }
                    #endregion
                }
                catch
                {

                }
                DropBombIfNearby();
                //Csabi(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY);
            }
        }
        public static void Csabi(float x, float y)
        {
            for (int i = 0; i < mapItems.GetLength(0); i++)
            {
                for (int j = 0; j < mapItems.GetLength(1); j++)
                {
                    char c = XMLFileProcessor.Map[i, j];
                    if (c == XMLFileProcessor.MyPlayer.TileChar)
                        c = 'Z';
                    //if ((c <= '9' && c >= '0') || (c <= 'H' && c >= 'A'))
                    //{
                    //    c = 'Z';
                    //}
                    mapItems[i, j] = new MapItem(i, j, c);
                }
            }
            int sourceX = (int)x;
            int sourceY = (int)y;



            //todo danger
            if (true)
            {

                char[] objects = new char[4];
                int[] objDist = new int[4];
                int longestDist = 0;

                int destDirX = 0;
                int destDirY = 0;

                ///////////// lefele
                int currentValue = mapItems[sourceX, sourceY].Value;
                int ii = sourceY;
                while (ii < mapItems.GetLength(1) && mapItems[sourceX, ii].Type == 'Z') ii++;
                if (ii < mapItems.GetLength(1)) objects[0] = mapItems[sourceX, ii].Type;


                objDist[0] = ii - (int)y;
                longestDist = ii - (int)y;
                destDirX = 0;
                destDirY = 1;
                ii = sourceY;
                while (ii >= 0 && mapItems[sourceX, ii].Type == 'Z') ii--;
                if ((int)y - ii > longestDist)
                {
                    longestDist = (int)y - ii;
                    objDist[1] = (int)y - ii;
                    destDirX = 0;
                    destDirY = -1;
                }
                /////// <-
                if (ii >= 0) objects[1] = mapItems[sourceX, ii].Type;



                ii = sourceX;
                while (ii < mapItems.GetLength(0) && mapItems[ii, sourceY].Type == 'Z') ii++;
                if (ii - (int)x > longestDist)
                {
                    longestDist = ii - (int)x;
                    objDist[2] = ii - (int)x;
                    destDirX = 1;
                    destDirY = 0;
                }
                // down
                if (ii < mapItems.GetLength(0)) objects[2] = mapItems[ii, sourceY].Type;


                ii = sourceX;
                while (ii >= 0 && mapItems[ii, sourceY].Type == 'Z') ii--;
                if ((int)x - ii > longestDist)
                {
                    longestDist = (int)x - ii;
                    objDist[3] = (int)x - ii;
                    destDirX = -1;
                    destDirY = 0;
                }
                if (ii >= 0) objects[3] = mapItems[sourceX, ii].Type;


                if (objects.Any(z => "0123456789ABCDEFGH".Contains(z)))
                {

                    int mindDist = int.MaxValue;
                    if ("0123456789ABCDEFGH".Contains(objects[0]))
                    {
                        destDirX = 0;
                        destDirY = 1;
                        if (objDist[0] < mindDist)
                        {
                            mindDist = objDist[0];
                        }
                    }
                    if ("0123456789ABCDEFGH".Contains(objects[1]))
                    {
                        if (objDist[1] < mindDist)
                        {
                            destDirX = 0;
                            destDirY = -1;
                            mindDist = objDist[1];
                        }
                    }
                    if ("0123456789ABCDEFGH".Contains(objects[2]))
                    {
                        if (objDist[2] < mindDist)
                        {
                            destDirX = 1;
                            destDirY = 0;
                            mindDist = objDist[2];
                        }

                    }
                    if ("0123456789ABCDEFGH".Contains(objects[3]))
                    {
                        if (objDist[3] < mindDist)
                        {
                            destDirX = -1;
                            destDirY = 0;
                            mindDist = objDist[3];
                        }
                    }
                    if (mindDist <= 1)
                    {
                        Commands.Stop();
                        Commands.Bomb();
                    }
                }
                if (XMLFileProcessor.MyPlayer.State.Equals("Standing"))
                {

                    if (destDirX == -1)
                    {
                        Commands.Left();
                    }
                    else if (destDirX == 1)
                    {
                        Commands.Right();
                    }
                    else if (destDirY == -1)
                    {
                        Commands.Up();
                    }
                    else if (destDirY == 1)
                    {
                        Commands.Down();
                    }
                }
            }
        }
        static MapItem SearchForSafeSpot()
        {
            MapItem result = null;
            Player player = XMLFileProcessor.MyPlayer;
            for (int i = 0; i < 5; i++)
            {
                if (player.PosX + i < mapItems.GetLength(0) && mapItems[(int)player.PosX + i, (int)player.PosY].Type == 'Z' && !Danger((int)player.PosX + i, (int)player.PosY))
                {
                    return mapItems[(int)player.PosX + i, (int)player.PosY];
                }
                if (player.PosX - i >= 0 && mapItems[(int)player.PosX - i, (int)player.PosY].Type == 'Z' && !Danger((int)player.PosX - i, (int)player.PosY))
                {
                    return mapItems[(int)player.PosX - i, (int)player.PosY];
                }
                if (player.PosY + i < mapItems.GetLength(0) && mapItems[(int)player.PosX, (int)player.PosY + i].Type == 'Z' && !Danger((int)player.PosX, (int)player.PosY + i))
                {
                    return mapItems[(int)player.PosX, (int)player.PosY + i];
                }
                if (player.PosY - i >= 0 && mapItems[(int)player.PosX, (int)player.PosY - i].Type == 'Z' && !Danger((int)player.PosX, (int)player.PosY - i))
                {
                    return mapItems[(int)player.PosX, (int)player.PosY - i];
                }
            }
            return result;
        }
        static void DropBombIfNearby()
        {
            int posX = (int)XMLFileProcessor.MyPlayer.PosX;
            int posY = (int)XMLFileProcessor.MyPlayer.PosY;
            int currentMapValue = mapItems[posX, posY].Value;
            if (currentMapValue > 0)
            {
                MapItem nextMapItem = null;
                if (posY - 1 >= 0 && mapItems[posX, posY - 1].Value == currentMapValue - 1)
                    nextMapItem = mapItems[posX, posY - 1];
                if (posX + 1 < mapItems.GetLength(0) && mapItems[posX + 1, posY].Value == currentMapValue - 1)
                    nextMapItem = mapItems[posX + 1, posY];
                if (posY + 1 < mapItems.GetLength(0) && mapItems[posX, posY + 1].Value == currentMapValue - 1)
                    nextMapItem = mapItems[posX, posY + 1];
                if (posX - 1 >= 0 && mapItems[posX - 1, posY].Value == currentMapValue - 1)
                    nextMapItem = mapItems[posX - 1, posY];

                if (nextMapItem != null)
                {
                    bool isEnemyNearby = false;
                    string enemyString = "0123456789ABCDEFGH";
                    int bombSize = XMLFileProcessor.MyPlayer.BombSize;

                    if (posX == nextMapItem.X)
                    {
                        int newPosY = posY - 1;
                        while (newPosY >= 0 &&
                               !isEnemyNearby &&
                               mapItems[posX, newPosY].Type != 'W' &&
                               posY - newPosY <= bombSize)
                        {
                            isEnemyNearby = enemyString.Contains(XMLFileProcessor.Map[posX, newPosY]);
                            newPosY--;
                        }

                        newPosY = posY + 1;
                        while (newPosY < mapItems.GetLength(1) &&
                               !isEnemyNearby &&
                               mapItems[posX, newPosY].Type != 'W' &&
                               newPosY - posY <= bombSize)
                        {
                            isEnemyNearby = enemyString.Contains(XMLFileProcessor.Map[posX, newPosY]);
                            newPosY++;
                        }
                    }
                    else if (posY == nextMapItem.Y)
                    {
                        int newPosX = posX - 1;
                        while (newPosX >= 0 &&
                               !isEnemyNearby &&
                               mapItems[newPosX, posY].Type != 'W' &&
                               posX - newPosX <= bombSize)
                        {
                            isEnemyNearby = enemyString.Contains(XMLFileProcessor.Map[newPosX, posY]);
                            newPosX--;
                        }

                        newPosX = posX + 1;
                        while (newPosX < mapItems.GetLength(0) &&
                               !isEnemyNearby &&
                               mapItems[newPosX, posY].Type != 'W' &&
                               newPosX - posX <= bombSize)
                        {
                            isEnemyNearby = enemyString.Contains(XMLFileProcessor.Map[newPosX, posY]);
                            newPosX++;
                        }
                    }

                    if (isEnemyNearby)
                    {
                        Commands.Bomb();
                        Commands.Reverse();
                    }
                }
            }
        }
        static int ClosestPowerUp()
        {
            if (XMLFileProcessor.Powerups.Count > 0)
            {
                int min = 0;
                float minWid = CalculateWidth(XMLFileProcessor.Powerups[0]);
                for (int i = 1; i < XMLFileProcessor.Powerups.Count; i++)
                {
                    float currWid = CalculateWidth(XMLFileProcessor.Powerups[i]);
                    if (currWid < minWid)
                    {
                        minWid = currWid;
                        min = i;
                    }
                }
                return min;
            }
            return -1;
        }
        static float CalculateWidth(Powerup powerup)
        {
            float res = 0;
            res += Math.Abs(powerup.PosX - XMLFileProcessor.MyPlayer.PosX);
            res += Math.Abs(powerup.PosY - XMLFileProcessor.MyPlayer.PosY);
            return res;
        }
        public static bool Danger(int x, int y) //használhatjuk a jelenlegi pozíciónk ellenőrzésére, vagy úticél ellenőrzésre is a paraméterekkel
        {
            foreach (Flame item in XMLFileProcessor.Flames)
            {
                if ((int)(item.PosX) == x && (int)(item.PosY) == y)
                {
                    return true;
                }
            }
            foreach (Bomb item in XMLFileProcessor.Bombs)
            {
                int bombsize = (XMLFileProcessor.Players.Find(g => g.ID == item.PlayerID)).BombSize;
                if ((int)(item.PosX + bombsize) >= x && (int)(item.PosX - bombsize) <= x && (int)(item.PosY) == y)
                {
                    return true;
                }
                if ((int)(item.PosY + bombsize) >= y && (int)(item.PosY - bombsize) <= y && (int)(item.PosX) == x)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool PathFinder(float srcX, float srcY, float dstX, float dstY)
        {
            float speed = XMLFileProcessor.MyPlayer.WalkSpeed;
            if (tdX != srcX && tdY != srcY)
            {
                tdX = tdY = -1;
            }
            if (tdX != -1)
            {
                if (Math.Abs(srcX - tdX) < 1.37 * (speed / 0.25) && Math.Abs(srcY - tdY) < 1.37 * (speed / 0.25))
                {
                    Commands.Stop();
                    tdX = tdY = -1;
                }
            }
            if (srcX != dstX || srcY != dstY)
            {

                int sourceX = (int)srcX;
                int sourceY = (int)srcY;
                int destinationX = (int)dstX;
                int destinationY = (int)dstY;
                mapItems[destinationX, destinationY].Value = 0;
                ProcessTile(destinationX, destinationY);
                while (toProcess.Count > 0)
                {
                    MapItem actualItem = toProcess.Pop();
                    ProcessTile(actualItem.X, actualItem.Y);
                }
                if (tdX != -1)
                {
                    if (tdX < sourceX)
                    {
                        Commands.Stop();
                        Commands.Left();
                    }
                    else if (tdX > sourceX)
                    {
                        Commands.Stop();
                        Commands.Right();
                    }
                    else if (tdY < sourceY)
                    {
                        Commands.Stop();
                        Commands.Up();
                    }
                    else if (tdY > sourceY)
                    {
                        Commands.Stop();
                        Commands.Down();
                    }
                }
                if (tdX == -1)
                {
                    if (mapItems[sourceX, sourceY].Value - mapItems[sourceX - 1, sourceY].Value == 1)
                    {
                        Commands.Stop();
                        int i = 0;
                        while (sourceX - i - 1 >= 0 && mapItems[sourceX - i, sourceY].Value - mapItems[sourceX - i - 1, sourceY].Value == 1)
                        {
                            i++;
                        }
                        tdX = sourceX - i;
                        tdY = sourceY;
                        Commands.Left();
                        return true;
                    }
                    else if (mapItems[sourceX, sourceY].Value - mapItems[sourceX + 1, sourceY].Value == 1)
                    {
                        Commands.Stop();
                        int i = 0;
                        while (sourceX + i + 1 < mapItems.GetLength(0) && mapItems[sourceX + i, sourceY].Value - mapItems[sourceX + i + 1, sourceY].Value == 1)
                        {
                            i++;
                        }
                        tdX = sourceX + i;
                        tdY = sourceY;
                        Commands.Right();
                        return true;
                    }
                    else if (mapItems[sourceX, sourceY].Value - mapItems[sourceX, sourceY + 1].Value == 1)
                    {
                        Commands.Stop();
                        int i = 0;
                        while (sourceY + i + 1 < mapItems.GetLength(1) && mapItems[sourceX, sourceY + i].Value - mapItems[sourceX, sourceY + i + 1].Value == 1)
                        {
                            i++;
                        }
                        tdY = sourceY + i;
                        tdX = sourceX;
                        Commands.Down();
                        return true;
                    }
                    else if (mapItems[sourceX, sourceY].Value - mapItems[sourceX, sourceY - 1].Value == 1)
                    {
                        Commands.Stop();
                        int i = 0;
                        while (sourceY - i - 1 >= 0 && mapItems[sourceX, sourceY - i].Value - mapItems[sourceX, sourceY - i - 1].Value == 1)
                        {
                            i++;
                        }
                        tdY = sourceY - i;
                        tdX = sourceX;
                        Commands.Up();
                        return true;
                    }
                }
            }
            return false;
        }

        static void ProcessTile(int x, int y)
        {
            if (x + 1 < mapItems.GetLength(0) && mapItems[x + 1, y].Type == 'Z' && mapItems[x + 1, y].Value > mapItems[x, y].Value)
            {
                mapItems[x + 1, y].Value = mapItems[x, y].Value + 1;
                toProcess.Push(mapItems[x + 1, y]);
            }
            if (y + 1 < mapItems.GetLength(1) && mapItems[x, y + 1].Type == 'Z' && mapItems[x, y + 1].Value > mapItems[x, y].Value)
            {
                mapItems[x, y + 1].Value = mapItems[x, y].Value + 1;
                toProcess.Push(mapItems[x, y + 1]);
            }
            if (x - 1 >= 0 && mapItems[x - 1, y].Type == 'Z' && mapItems[x - 1, y].Value > mapItems[x, y].Value)
            {
                mapItems[x - 1, y].Value = mapItems[x, y].Value + 1;
                toProcess.Push(mapItems[x - 1, y]);
            }
            if (y - 1 >= 0 && mapItems[x, y - 1].Type == 'Z' && mapItems[x, y - 1].Value > mapItems[x, y].Value)
            {
                mapItems[x, y - 1].Value = mapItems[x, y].Value + 1;
                toProcess.Push(mapItems[x, y - 1]);
            }
        }
    }
}
