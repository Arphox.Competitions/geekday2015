﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class MapItem
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public int Value { get; set; }
        public char Type { get; private set; }
        public MapItem(int x, int y, char type)
        {
            X = x;
            Y = y;
            Type = type;
            Value = int.MaxValue;
        }
    }
}
