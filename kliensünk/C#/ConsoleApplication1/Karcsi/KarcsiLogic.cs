﻿using karesz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public static class KarcsiLogic
    {
        public static Random r = new Random();

        public static bool CanIGoThatWay(Direction d)
        {
            Player me = XMLFileProcessor.MyPlayer;

            if (d == Direction.Right)
            {
                if (XMLFileProcessor.Map[(int)Math.Round(me.PosX) + 1, (int)Math.Round(me.PosY)] != 'V' &&
                    XMLFileProcessor.Map[(int)Math.Round(me.PosX) + 1, (int)Math.Round(me.PosY)] != 'W')
                {
                    return true;
                }
                else
                    return false;
            }
            if (d == Direction.Left)
            {
                if (XMLFileProcessor.Map[(int)Math.Round(me.PosX) - 1, (int)Math.Round(me.PosY)] != 'V' &&
                    XMLFileProcessor.Map[(int)Math.Round(me.PosX) - 1, (int)Math.Round(me.PosY)] != 'W')
                {
                    return true;
                }
                else
                    return false;
            }
            if (d == Direction.Up)
            {
                if (XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) - 1] != 'V' &&
                    XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) - 1] != 'W')
                {
                    return true;
                }
                else
                    return false;
            }
            if (d == Direction.Down)
            {
                if (XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) + 1] != 'V' &&
                    XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) + 1] != 'W')
                {
                    return true;
                }
                else
                    return false;
            }
            return false;
        }
        public static bool ShouldIGoThatWay(Direction d)
        {
            Player me = XMLFileProcessor.MyPlayer;

            if (d == Direction.Right)
            {
                if (XMLFileProcessor.Map[(int)Math.Round(me.PosX) + 1, (int)Math.Round(me.PosY)] != 'X' &&
                    XMLFileProcessor.Map[(int)Math.Round(me.PosX) + 1, (int)Math.Round(me.PosY)] != 'Y')
                {
                    return true;
                }
                else
                    return false;
            }
            if (d == Direction.Left)
            {
                if (XMLFileProcessor.Map[(int)Math.Round(me.PosX) - 1, (int)Math.Round(me.PosY)] != 'X' &&
                    XMLFileProcessor.Map[(int)Math.Round(me.PosX) - 1, (int)Math.Round(me.PosY)] != 'Y')
                {
                    return true;
                }
                else
                    return false;
            }
            if (d == Direction.Up)
            {
                if (XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) - 1] != 'X' &&
                    XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) - 1] != 'Y')
                {
                    return true;
                }
                else
                    return false;
            }
            if (d == Direction.Down)
            {
                if (XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) + 1] != 'X' &&
                    XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) + 1] != 'Y')
                {
                    return true;
                }
                else
                    return false;
            }
            return false;
        }

        //public static void WhereCanIGo()
        //{
        //    Player me = XMLFileProcessor.MyPlayer;

        //    bool leftIsSafe = ShouldIGoThatWay(Direction.Left);
        //    bool rightIsSafe = ShouldIGoThatWay(Direction.Right);
        //    bool upIsSafe= ShouldIGoThatWay(Direction.Up);
        //    bool downIsSafe = ShouldIGoThatWay(Direction.Down);

        //    bool leftIsPossible = CanIGoThatWay(Direction.Left);
        //    bool rightIsPossible = CanIGoThatWay(Direction.Right);
        //    bool upIsPossible = CanIGoThatWay(Direction.Up);
        //    bool downIsPossible = CanIGoThatWay(Direction.Down);

        //    #region Is something dangerous on me?
        //    if (XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY)] == 'Y' || //If I have flame or bomb on me
        //        XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY)] == 'X')
        //    {
        //        //Where Should I go?
        //        if (leftIsSafe) { Commands.Left(); }
        //        else if (rightIsSafe) { Commands.Right(); }
        //        else if (upIsSafe) { Commands.Up(); }
        //        else if (downIsSafe) { Commands.Down(); }

        //        //Okay, I am surrounded by flames or bombs. Let's just run where I can:
        //        if (leftIsPossible) { Commands.Left(); }
        //        else if (rightIsPossible) { Commands.Right(); }
        //        else if (upIsPossible) { Commands.Up(); }
        //        else if (downIsPossible) { Commands.Down(); }
        //    }
        //    #endregion

        //    #region Is something dangerous next to me?

        //    if (!leftIsSafe) //Danger at left hand
        //    {

        //    }

        //    #endregion
        //}


        public static bool RouteGenerated = false;
        public static List<string> CommandList = new List<string>();

        public static Direction GetAPossibleDirection()
        {
            bool leftIsSafe = ShouldIGoThatWay(Direction.Left);
            bool rightIsSafe = ShouldIGoThatWay(Direction.Right);
            bool upIsSafe = ShouldIGoThatWay(Direction.Up);
            bool downIsSafe = ShouldIGoThatWay(Direction.Down);

            bool leftIsPossible = CanIGoThatWay(Direction.Left);
            bool rightIsPossible = CanIGoThatWay(Direction.Right);
            bool upIsPossible = CanIGoThatWay(Direction.Up);
            bool downIsPossible = CanIGoThatWay(Direction.Down);




            if (leftIsSafe)
                return Direction.Left;
            if (rightIsSafe)
                return Direction.Right;
            if (upIsSafe)
                return Direction.Up;
            if (downIsSafe)
                return Direction.Down;

            if (leftIsPossible)
                return Direction.Left;
            if (rightIsPossible)
                return Direction.Right;
            if (upIsPossible)
                return Direction.Up;
            if (downIsPossible)
                return Direction.Down;

            return Direction.Down;
        }
        public static void GoRandomWay()
        {
            bool[] possibilities = new bool[] { CanIGoThatWay(Direction.Left), CanIGoThatWay(Direction.Right), CanIGoThatWay(Direction.Up), CanIGoThatWay(Direction.Down) };
            int[] rndIndexes = new int[] { 0, 1, 2, 3 };
            rndIndexes = rndIndexes.OrderBy(x => r.Next()).ToArray();
            int cnt = 0;
            for (int i = rndIndexes[0]; cnt < 4; i = rndIndexes[cnt])
            {
                if (possibilities[i])
                {
                    if (i == 0)
                    {
                        Commands.Left();
                        break;
                    }
                    else if (i == 1)
                    {
                        Commands.Right();
                        break;
                    }
                    else if (i == 2)
                    {
                        Commands.Up();
                        break;
                    }
                    else if (i == 3)
                    {
                        Commands.Down();
                        break;
                    }
                }

                cnt++;
            }
        }
        public static void KarcsiTACTIC()
        {
            Player me = XMLFileProcessor.MyPlayer;

            if (me == null)
            {
                TcpUdp.Send("/STOP");

                Console.WriteLine("Player = null ...\n\nRecieved player list:");
                for (int i = 0; i < XMLFileProcessor.Players.Count; i++)
                {
                    Console.WriteLine(XMLFileProcessor.Players[i].NickName);
                }
                return;
            }

            switch (r.Next(0, 4))
            {
                //jobbra
                case 0:
                    if (XMLFileProcessor.Map[(int)Math.Round(me.PosX) + 1, (int)Math.Round(me.PosY)] == 'Z')
                    { Commands.Right(); }
                    else
                    {
                        GoRandomWay();
                    }
                    break;

                case 1:
                    if (XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) + 1] == 'Z')
                    { Commands.Down(); }
                    else
                    {
                        GoRandomWay();
                    }
                    break;

                case 2:
                    if (XMLFileProcessor.Map[(int)Math.Round(me.PosX) - 1, (int)Math.Round(me.PosY)] == 'Z')
                    { Commands.Left(); }
                    else
                    {
                        GoRandomWay();
                    }
                    break;

                case 3:
                    if (XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) - 1] == 'Z')
                    { Commands.Up(); }
                    else
                    {
                        GoRandomWay();
                    }
                    break;
            }
            bool bombakint = false;
            //Van-e enemy rajtam vagy előttem/mögöttem?
            if ( //Rajtam?
                (XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY)] >= '0' &&
                XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY)] <= 'H' &&
                XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY)] != XMLFileProcessor.MyPlayer.TileChar)
                || //Felettem?
                (XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) - 1] >= '0' &&
                XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) - 1] <= 'H')
                || //Alattam?
                (XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) + 1] >= '0' &&
                XMLFileProcessor.Map[(int)Math.Round(me.PosX), (int)Math.Round(me.PosY) + 1] <= 'H')
                || //Balra tőlem?
                (XMLFileProcessor.Map[(int)Math.Round(me.PosX) - 1, (int)Math.Round(me.PosY)] >= '0' &&
                XMLFileProcessor.Map[(int)Math.Round(me.PosX) - 1, (int)Math.Round(me.PosY)] <= 'H')
                || //Alattam?
                (XMLFileProcessor.Map[(int)Math.Round(me.PosX) + 1, (int)Math.Round(me.PosY)] >= '0' &&
                XMLFileProcessor.Map[(int)Math.Round(me.PosX) + 1, (int)Math.Round(me.PosY)] <= 'H')
                )
            {
                //Akkor rakjunk le bombát
                Commands.Bomb();
                //Commands.Reverse();
                bombakint = true;
            }

            if (r.Next(0, 5) == 0 && !bombakint)
            {
                Commands.Bomb();
                //Commands.Reverse();
                
            }

        }

        public static void DirectionToCommand(Direction d)
        {
            if (d == Direction.Down)
                Commands.Down();
            else if (d == Direction.Up)
                Commands.Up();
            else if (d == Direction.Left)
                Commands.Left();
            else
                Commands.Right();
        }
    }

    public enum Direction
    {
        Nothing, Left, Right, Up, Down
    }
}
