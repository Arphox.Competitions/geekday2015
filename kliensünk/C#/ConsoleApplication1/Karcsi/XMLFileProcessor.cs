﻿using System;
using System.Collections.Generic;
using System.IO;
using ConsoleApplication1;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Xml.Linq;

namespace karesz
{
    public static class XMLFileProcessor
    {
        static List<XMLFile> xmlFiles = new List<XMLFile>();
        public static List<XMLFile> XmlFiles
        {
            get
            {
                return xmlFiles;
            }
        }

        public static List<Player> Players { get; private set; }

        /// <summary>
        /// Walls (W, V), Space (Z)
        /// </summary>
        public static List<GameItem> OtherGameItems { get; private set; }
        public static List<Bomb> Bombs { get; private set; }
        public static List<Flame> Flames { get; private set; }
        public static List<Powerup> Powerups { get; private set; }
        public static char[,] Map { get; private set; }
        public static Player MyPlayer { get; private set; }

        public static Stopwatch SW { get; private set; }

        static XMLFileProcessor()
        {
            CONSTRUCTOR();

            SW = new Stopwatch();
        }
        static void CONSTRUCTOR()
        {
            Bombs = new List<Bomb>();
            Flames = new List<Flame>();
            Powerups = new List<Powerup>();
            Map = new char[20, 20];
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    Map[i, j] = '?';
                }
            }

            OtherGameItems = new List<GameItem>();
        }

        static int LatestSendIndex { get; set; }

        public static void NewPacket(byte[] packetBytes)
        {
            int SendIndex = BitConverter.ToInt32(packetBytes, 0);

            //Ha már van ilyen SendIndexű XML fájl, amit elkezdtünk
            if (XmlFiles.Exists(x => x.SendIndex == SendIndex))
            {
                XMLFile existingFile = XmlFiles.Find(x => x.SendIndex == SendIndex);
                existingFile.NewPacket(packetBytes);
            }
            else //Még nincs ilyen sendindexű kaki
            {
                XMLFile a = new XMLFile(SendIndex);
                XmlFiles.Add(a);
                a.NewPacket(packetBytes);
            }
        }

        public static void Actualize(string xmlString, int SendIndex)
        {
            if (SendIndex > LatestSendIndex)
            {
                CONSTRUCTOR();
                XDocument xDoc;
                try
                {
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.Clear();
                    Console.WriteLine("Actualized: {0} => {1}\t{2} items\t (Delta:{3})", LatestSendIndex, SendIndex, xmlFiles.Count, SW.Elapsed);

                    xDoc = XDocument.Parse(xmlString);

                    Players = xDoc.Root.Elements("Player").Select(x => new Player(x)).ToList();
                    MyPlayer = Players.Find(x => x.NickName == Settings.NickName);

                    if (MyPlayer == null)
                    {
                        Debug.WriteLine("MyPlayer = null ...    SendIndex = " + SendIndex);
                        Commands.Stop();
                    }


                    #region GameItems and Powerups
                    List<XElement> gameItemTags = xDoc.Root.Elements("GameItem").ToList();

                    for (int i = 0; i < gameItemTags.Count; i++)
                    {
                        if (gameItemTags[i].Element("IsPickable").Value == "true")
                        {
                            Powerups.Add(new Powerup(gameItemTags[i]));
                        }
                        else
                        {
                            OtherGameItems.Add(new GameItem(gameItemTags[i]));
                        }
                    }
                    #endregion

                    #region Bombs and Flames
                    List<XElement> bombTags = xDoc.Root.Elements("Bomb").ToList();
                    int count = bombTags.Count();
                    for (int i = 0; i < count; i++)
                    {
                        if (Convert.ToChar(int.Parse(bombTags[i].Element("TileChar").Value)) == 'Y')
                        {
                            Bombs.Add(new Bomb(bombTags[i]));
                        }
                        else
                        {
                            Flames.Add(new Flame(bombTags[i]));
                        }
                    }
                    #endregion

                    #region Map Generation

                    //Other game items:
                    for (int i = 0; i < OtherGameItems.Count; i++)
                        Map[OtherGameItems[i].PosX, OtherGameItems[i].PosY] = OtherGameItems[i].TileChar;
                    //Players:
                    for (int i = 0; i < Players.Count; i++)
                        Map[(int)Math.Round(Players[i].PosX), (int)Math.Round(Players[i].PosY)] = Players[i].TileChar;
                    //Bombs:
                    for (int i = 0; i < Bombs.Count; i++)
                        Map[Bombs[i].PosX, Bombs[i].PosY] = 'Y';
                    //Flames:
                    for (int i = 0; i < Flames.Count; i++)
                        Map[Flames[i].PosX, Flames[i].PosY] = 'X';
                    //Powerups:
                    for (int i = 0; i < Powerups.Count; i++)
                        Map[Powerups[i].PosX, Powerups[i].PosY] = Powerups[i].TileChar;


                    #endregion

                    MapToConsole();

                    LatestSendIndex = SendIndex;

                    ConsoleApplication1.GameLogic.CalculateMoves();

                    #region 1 percnél régebbi XMLFile-ok törlése
                    for (int i = xmlFiles.Count - 1; i >= 0; i--)
                    {
                        if (DateTime.Now - xmlFiles[i].TimeStamp > new TimeSpan(0, 1, 0))
                        {
                            xmlFiles.RemoveAt(i);
                        }
                    }

                    #endregion
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    SW.Restart();
                }
            }
        }

        public static void MapToConsole()
        {
            Player followTarget = MyPlayer;
            //Player followTarget = Players.Find(x => x.NickName == "szar2");
            char followChar = '.';

            if (followTarget == null)
            {
                followChar = '.';
                return;
            }


            followChar = followTarget.TileChar;

            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    if (Map[j, i] == 'Z')
                    {
                        Console.BackgroundColor = ConsoleColor.DarkGreen;
                        Console.Write(' ');
                    }
                    else if (Map[j, i] == 'W') //NonExplodable
                    {
                        Console.BackgroundColor = ConsoleColor.DarkGray;
                        Console.Write(' ');
                    }
                    else if (Map[j, i] == 'V') //Explodable
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Write(' ');
                    }
                    else if (Map[j, i] == followChar)
                    {
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(Map[j, i]);
                        Console.ForegroundColor = ConsoleColor.Gray;
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Write(Map[j, i]);
                    }
                }
                Console.WriteLine();
            }

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }

    public class XMLFile
    {
        public XMLFile(int sendIndex)
        {
            //XMLReadyEVENT += CreateXML;
            this.sendIndex = sendIndex;
            this.TimeStamp = DateTime.Now;
        }

        //public event CreateXML XMLReadyEVENT;

        //Private fields
        string[] messagePieces;
        bool MessagePiecesArray_Created;
        /// <summary>
        /// Átjött-e már minden package a message-hez?
        /// </summary>
        bool MessageReady
        {
            get
            {
                for (int i = 0; i < messagePieces.Length; i++)
                {
                    if (messagePieces[i] == null)
                        return false;
                }
                return true;
            }
        }

        #region Public fields
        int sendIndex;
        /// <summary>
        /// Amelyik SendIndexhez tartozik az xml fájl
        /// </summary>
        public int SendIndex
        {
            get
            {
                return sendIndex;
            }
        }

        bool usable;
        /// <summary>
        /// Megadja, hogy kész-e már az XML fájl
        /// </summary>
        public bool Usable
        {
            get
            {
                return usable;
            }
        }

        public DateTime TimeStamp { get; private set; }

        string createdXMLstring;
        public string CreatedXMLstring
        {
            get
            {
                return createdXMLstring;
            }
        }
        #endregion

        #region Private methods
        void CreateXML()
        {
            StringBuilder fullmessage = new StringBuilder();

            //XML header berakása:
            fullmessage.Append("<?xml version=\"1.0\"?>\r\n");

            //XML body berakása:
            for (int i = 0; i < messagePieces.Length; i++)
            {
                fullmessage.Append(messagePieces[i]);
            }

            //Végéről elindulunk és töröljük a 00 hexa értékeket:
            int eddig = fullmessage.Length - 1;
            while (fullmessage[eddig] != '>')
                eddig--;

            fullmessage.Remove(eddig + 1, fullmessage.Length - eddig - 1);

            if (fullmessage[fullmessage.Length - 1] != '>')
            {

            }

            this.createdXMLstring = fullmessage.ToString();
            this.usable = true;

            //Actualization
            XMLFileProcessor.Actualize(this.CreatedXMLstring, this.SendIndex);
        }
        #endregion

        //Public methods
        public void NewPacket(byte[] packetBytes)
        {
            int PacketIndex = BitConverter.ToInt32(packetBytes, 4);

            if (!MessagePiecesArray_Created)
            {
                #region CreateMessage(packetBytes);
                int SendIndex = BitConverter.ToInt32(packetBytes, 0);
                int PacketCount = BitConverter.ToInt32(packetBytes, 8);

                messagePieces = new string[PacketCount];

                MessagePiecesArray_Created = true;
                #endregion
            }

            //Body process:
            StringBuilder content = new StringBuilder(1000);

            for (int i = 12; i < 1012; i++)
            {
                content.Append(Convert.ToChar(packetBytes[i]));
            }

            try
            {
                messagePieces[PacketIndex - 1] = content.ToString();
            }
            catch
            {

            }
            if (MessageReady)
            {
                CreateXML();
            }
        }
    }
}
