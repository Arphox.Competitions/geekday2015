Please note that from 14:30 
- you can only use one client per team!
- no more new-nick registration after 14:30!
- the nick of the user should somehow resemble the team name!
(OR: somehow we should know that team X is playing with nick Z)

Also, IMPORTANT CHANGE IN THE POINTS SYSTEM:

To avoid the fact that until now, "standing still" was a better strategy than "trying to blow things up", we changed the way points are given in the game: from the next update (and in the final game) there will be NO NEGATIVE POINTS for losing a life, no matter how that life was lost.

Also, please note that we will manually re-start the router around 3pm. We do this to avoid the regular breakdown that seems to be happening once in every 4-5 hours. That means: no internet for ~20 minutes (this is the time the damn router needs to get everything together)