﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekDay4_Dyna.Game
{
    public class Bomb : GameItem
    {
        public static char BOMBCHAR = 'Y';
        public static char FLAMECHAR = 'X';
        
        public double TimeLeft {  get; set; }
        public Player Owner { get; set; }
    }
}
