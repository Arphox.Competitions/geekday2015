﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace GeekDay4_Dyna.Classes
{
    class MyMulticastServer
    {
        Socket s;
        int sendIndex=0;

        public MyMulticastServer(string ipAddr, int port)
        {
            s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPAddress ip = IPAddress.Parse(ipAddr);
            s.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(ip));
            s.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 3);
            IPEndPoint ipep = new IPEndPoint(ip, port);
            s.SendBufferSize = 128000;
            s.Connect(ipep);

        }

        public void Send(int num)
        {
            byte[] b = new byte[10];
            for (int x = 0; x < b.Length; x++) b[x] = (byte)(x + 65 + num);
            s.Send(b, b.Length, SocketFlags.None);
        }

        byte[] pkgBuffer = new byte[1012];
        byte[] pkgCountBuffer = new byte[4];
        byte[] pkgIndexBuffer = new byte[4];
        byte[] sendIndexBuffer = new byte[4];

        public void Send(string str)
        {
            int pkgIndex = 0;
            sendIndex++;
            sendIndexBuffer = BitConverter.GetBytes(sendIndex);
            int pkgCount = (int)Math.Ceiling((double)str.Length / 1000);
            pkgCountBuffer = BitConverter.GetBytes(pkgCount);
            for (int i = 0; i < str.Length; i += 1000)
            {
                Array.Clear(pkgBuffer, 0, pkgBuffer.Length);

                int len = i < str.Length - 1000 ? 1000 : str.Length - i;
                Encoding.ASCII.GetBytes(str, i, len, pkgBuffer, 12);

                pkgIndex++;
                pkgIndexBuffer = BitConverter.GetBytes(pkgIndex);
                Array.Copy(sendIndexBuffer, 0, pkgBuffer, 0, 4);
                Array.Copy(pkgIndexBuffer, 0, pkgBuffer, 4, 4);
                Array.Copy(pkgCountBuffer, 0, pkgBuffer, 8, 4);
                s.Send(pkgBuffer, pkgBuffer.Length, SocketFlags.None);
            }
        }
    }
}
