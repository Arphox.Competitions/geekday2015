﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace GeekDay4_Dyna.Classes
{
    public class MyTCPServer
    {
        const bool ALLOWREGISTER = false;

        public delegate void msgReceivedHandler(OneClient client, string msg);
        public event msgReceivedHandler msgReceived;
        public bool Server_Active = false;
        public Dispatcher GUIDispatcher { get; set; }

        TcpListener Server;
        Thread Server_Thread;
        public ObservableCollection<OneClient> Clients { get; private set; }
        //Dictionary<string, OneClient> Clients = new Dictionary<string, OneClient>();
        List<OneClient> PendingClients = new List<OneClient>();

        public MyTCPServer(int port = 6666)
        {
            Clients = new ObservableCollection<OneClient>();

            IPAddress all = new IPAddress(new byte[4] { 0, 0, 0, 0 });
            Server = new TcpListener(all, port);
            Server.Start();
            Server_Thread = new Thread(new ThreadStart(WaitClientsInThread));
            Server_Thread.Start();
        }

        public void Shutdown()
        {
            GUIDispatcher.BeginInvoke((Action)(() =>
            {
                Server_Active = false;
                foreach (var akt in Clients)
                {
                    SendSysMsg("*** KICK: " + akt.NickName + " ***");
                    akt.Send("*** SERVER SHUTDOWN ***");
                    akt.Close();
                }
                Clients.Clear();
            }));
            Server_Thread.Abort();
        }

        void WaitClientsInThread()
        {
            Server_Active = true;
            while (Server_Active)
            {
                if (Server.Pending())
                {
                    OneClient OC = new OneClient(Server.AcceptTcpClient());
                    OC.MessageReceived += Client_NormalMsg;
                    OC.ClientClosed += Client_Closed;
                    OC.Send("Welcome to the GeekDay server...\r\nPlease send your nickname and password first!\r\n\r\n-------------------------------\r\n\r\n");
                    GUIDispatcher.BeginInvoke((Action)(() =>
                                    {
                                        PendingClients.Add(OC);
                                    }));
                }
                Thread.Sleep(500);
            }
        }

        void Client_Closed(object sender, EventArgs e)
        {
            GUIDispatcher.BeginInvoke((Action)(() =>
            {
                Clients.Remove((sender as OneClient));
            }));
        }

        public void Client_NormalMsg(object sender, string msg)
        {
            OneClient client = sender as OneClient;
            if (Server_Active && msg.Trim().Length > 0)
            {
                if (msg[0] != '/')
                {
                    if (Clients.Contains(client))
                    {
                        Broadcast(String.Format("[{0}] {1}", client.NickName, msg));
                        msgReceived(client, msg);
                    }
                    else
                        client.Send("ERROR - PLEASE DO /LOGIN FIRST!");
                }
                else
                {
                    string[] cmd = msg.Split(' ');
                    switch (cmd[0])
                    {
                        case "/LOGIN":
                            if (VerifyLogin(cmd))
                            {
                                GUIDispatcher.BeginInvoke((Action)(() =>
                                                    {
                                                        PendingClients.Remove(client);
                                                        if (!Clients.Contains(client))
                                                        {
                                                            Clients.Add(client);
                                                        }
                                                    }));

                                client.NickName = cmd[1];
                                try
                                {
                                    client.IPAddr = IPAddress.Parse(cmd[3]);
                                    client.PortNum = int.Parse(cmd[4]);
                                }
                                catch (Exception)
                                {
                                }
                                client.Send("OK, successful auth!");
                                SendSysMsg("AUTH:" + client.NickName);
                                msgReceived(client, msg);
                            }
                            else
                            {
                                client.Send("LOGIN ERROR - PLEASE RETRY!");
                            }
                            break;
                        default:
                            string nick = client.NickName;
                            if (!String.IsNullOrEmpty(nick))
                                msgReceived(client, msg);
                            else
                                client.Send("PLEASE DO /LOGIN FIRST!");
                            break;
                    }
                }
            }
        }
        private void Broadcast(string message, string target = "*")
        {
            List<OneClient> clientsCopy = new List<OneClient>();
            GUIDispatcher.Invoke((Action)(()=>
            {
                clientsCopy.AddRange(Clients);
            }));
            foreach (OneClient akt in clientsCopy)
            {
                if (target == "*" || target == akt.NickName) akt.Send(message);
            }
        }
        public void SendUdpString(string msg)
        {
            List<OneClient> clientsCopy = new List<OneClient>();
            GUIDispatcher.Invoke((Action)(() =>
            {
                clientsCopy.AddRange(Clients);
            }));
            foreach (OneClient akt in clientsCopy)
            {
                akt.SendUdpString(msg);
            }
        }
        public void SendSysMsg(string msg)
        {
            Broadcast("[TCPSERVER] " + msg);
        }
        public void SendSysMsg(string msg, string target)
        {
            Broadcast("[TCPSERVER] " + msg, target);
        }

        private bool VerifyLogin(string[] cmd)
        {
            if (cmd.Length < 3) return false;
            if (cmd[1].Trim().Length < 3 || cmd[2].Trim().Length < 3) return false;
            string pw = Operations.IniFile.IniReadValue("users", cmd[1], "");
            if (pw == cmd[2]) return true;
            if (ALLOWREGISTER && (pw == "" || pw == null))
            {
                Operations.IniFile.IniWriteValue("users", cmd[1], cmd[2]);
                return true;
            }
            return false;
        }
    }

}
