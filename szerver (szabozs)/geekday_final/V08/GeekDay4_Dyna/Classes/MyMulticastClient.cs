﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GeekDay4_Dyna.Classes
{
    public class MyMulticastClient
    {
        public delegate void PacketReceivedEventHandler(object sender, int sendIndex, int pkgIndex, int pkgCount, string s);
        public event PacketReceivedEventHandler PacketReceived;
        public event EventHandler<string> MessageReceived;

        Socket s;
        Thread recv_thread;
        volatile bool isRunning;

        public MyMulticastClient(string ipAddr, int port)
        {
            isRunning = true;

            s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, port);
            s.Bind(ipep);
            IPAddress ip = IPAddress.Parse(ipAddr);

            //s.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(ip, IPAddress.Any));
            s.ReceiveBufferSize = 1024000;
            recv_thread = new Thread(ReceiveData);
            recv_thread.Start();
        }

        private void ReceiveData()
        {
            int i = 0;
            int pkgSize = 1012;
            byte[] buffer = new byte[pkgSize];
            while (isRunning)
            {
                //Console.WriteLine("... WAITING ..."+ i);
                i++;
                if (s.Available>=pkgSize)
                {
                    int num = s.Receive(buffer, pkgSize, SocketFlags.None);
                    int sendIndex = BitConverter.ToInt32(buffer, 0);
                    int pkgIndex = BitConverter.ToInt32(buffer, 4);
                    int pkgCount = BitConverter.ToInt32(buffer, 8);
                    string str = Encoding.ASCII.GetString(buffer, 12, pkgSize - 12);
                    //string msg = String.Format("Send {0}, Pkg {1} of {2}\n{3}", sendIndex, pkgIndex, pkgCount, str.Trim());
                    string msg = String.Format("Send {0}, Pkg {1} of {2}", sendIndex, pkgIndex, pkgCount);
                    //Console.WriteLine(msg);
                    //File.WriteAllBytes(String.Format("LOG\\send_{0:D3}_pkg_{1:D3}.raw", sendIndex, pkgIndex), buffer);
                    var h1 = MessageReceived;
                    if (h1 != null) { h1(this, msg); }

                    var h2 = PacketReceived;
                    if (h2 != null) { h2(this, sendIndex, pkgIndex, pkgCount, str); }
                }
                else
                {
                    System.Threading.Thread.Sleep(100);
                }
            }
        }

        public void Close()
        {
            isRunning = false;
            recv_thread.Abort();
        }

    }
}
