﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GeekDay4_Dyna.Classes
{
    public class OneClient : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(name));
        }


        public event EventHandler<string> MessageReceived;
        public event EventHandler ClientClosed;
        
        string nickName;
        public string NickName
        {
            get { return nickName; }
            set { nickName = value; OnPropertyChanged(); }
        }

        int clientNumber;
        public int ClientNumber
        {
            get { return clientNumber; }
            set { clientNumber = value; OnPropertyChanged(); }
        }

        int cmdNumber;

        public int CmdNumber
        {
            get { return cmdNumber; }
            set { cmdNumber = value; OnPropertyChanged(); }
        }

        public IPAddress IPAddr { get; set; }
        public int PortNum { get; set; }
        int sendIndex = 0;
        byte[] pkgBuffer = new byte[1012];
        byte[] pkgCountBuffer = new byte[4];
        byte[] pkgIndexBuffer = new byte[4];
        byte[] sendIndexBuffer = new byte[4];

        Socket sock;
        IPEndPoint endPoint;

        public void SendUdpString(string str)
        {
            if (IPAddr == null || PortNum == 0 || IPAddr.AddressFamily!=AddressFamily.InterNetwork) return;
            if (sock == null)
            {
                sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                sock.SendBufferSize = 1024000;
                endPoint = new IPEndPoint(IPAddr, PortNum);
            }
            int pkgIndex = 0;
            sendIndex++;
            sendIndexBuffer = BitConverter.GetBytes(sendIndex);
            int pkgCount = (int)Math.Ceiling((double)str.Length / 1000);
            pkgCountBuffer = BitConverter.GetBytes(pkgCount);
            for (int i = 0; i < str.Length; i += 1000)
            {
                Array.Clear(pkgBuffer, 0, pkgBuffer.Length);

                int len = i < str.Length - 1000 ? 1000 : str.Length - i;
                Encoding.ASCII.GetBytes(str, i, len, pkgBuffer, 12);

                pkgIndex++;
                pkgIndexBuffer = BitConverter.GetBytes(pkgIndex);
                Array.Copy(sendIndexBuffer, 0, pkgBuffer, 0, 4);
                Array.Copy(pkgIndexBuffer, 0, pkgBuffer, 4, 4);
                Array.Copy(pkgCountBuffer, 0, pkgBuffer, 8, 4);
                sock.SendTo(pkgBuffer, pkgBuffer.Length, SocketFlags.None, endPoint);
            }
        }

        BinaryReader BR;
        BinaryWriter BW;
        TcpClient innerClient;
        Thread clientThread;
        volatile bool clientActive;

        public OneClient(TcpClient c)
        {
            NickName = String.Empty;
            ClientNumber = -1;

            innerClient = c;

            innerClient.ReceiveTimeout = 0;
            innerClient.SendTimeout = 4000;

            BR = new BinaryReader(innerClient.GetStream());
            BW = new BinaryWriter(innerClient.GetStream());
            clientActive = true;
            clientThread = new Thread(new ThreadStart(run));
            clientThread.Start();
        }

        public void Send(string msg)
        {
            if (clientActive)
            {
                new Thread(() =>
                {
                    if (!BW.WriteMyString(msg))
                    {
                        Close();
                    }
                }).Start();
            }
        }

        public void Close()
        {
            clientActive = false;
            innerClient.Close();
            clientThread.Abort();
            if (ClientClosed != null)
            {
                ClientClosed(this, EventArgs.Empty);
            }
        }

        void run()
        {
            while (clientActive)
            {
                if (innerClient.Connected && innerClient.Client.Available > 0)
                {
                    string line = BR.ReadMyString();
                    if (line == "") continue;
                    var handler = MessageReceived;
                    if (handler != null)
                    {
                        handler(this, line);
                    }
                }
                //if (!Operations.IsConnected(Inner_Client.Client))
                if (!innerClient.Client.Connected)
                {
                    Close();
                }
                Thread.Sleep(50);
            }
        }
    }

}
