﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using GeekDay4_Dyna.Classes;
using System.IO;
using System.Windows.Threading;

namespace DynaClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MyTCPClient tcpClient;
        MyMulticastClient mcastClient;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            tcpClient = new MyTCPClient(txtHost.Text, int.Parse(txtPort.Text), txtUname.Text, txtUpass.Text, txtUdpHost.Text, txtUdpPort.Text);
            tcpClient.MessageReceived += tcpClient_MessageReceived;

            //mcastClient = new MyMulticastClient("224.5.6.7", 9999);
            mcastClient = new MyMulticastClient(txtUdpHost.Text, int.Parse(txtUdpPort.Text));
            mcastClient.MessageReceived += mcastClient_MessageReceived;
            mcastClient.PacketReceived += mcastClient_PacketReceived;
        }

        void mcastClient_PacketReceived(object sender, int sendIndex, int pkgIndex, int pkgCount, string s)
        {
            Dispatcher.Invoke(() =>
            {
                if (chkSaveUdp.IsChecked == true)
                {
                    File.WriteAllText(String.Format("LOG\\send_{0:D3}_pkg_{1:D3}_of_{2:D3}.raw", sendIndex, pkgIndex, pkgCount), s);
                }
            });
        }

        void mcastClient_MessageReceived(object sender, string e)
        {
            Dispatcher.Invoke(() =>
            {
                txtMcastMessages.Text = e + "\n";
                txtMcastMessages.ScrollToEnd();
            });
        }

        void tcpClient_MessageReceived(object sender, string e)
        {
            Dispatcher.Invoke(() =>
            {
                txtMessages.Text += e + "\n";
                txtMessages.ScrollToEnd();
            });
        }

        private void Send_Click(object sender, RoutedEventArgs e)
        {
            if (tcpClient != null)
            {
                tcpClient.SendMsg(txtCommand.Text);
            }
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (tcpClient != null)
            {
                tcpClient.Close();
                mcastClient.Close();
            }
        }

        Dictionary<char, string> commands = new Dictionary<char, string>()
        {
            {'W', "/UP"},
            {'S', "/DOWN"},
            {'A', "/LEFT"},
            {'D', "/RIGHT"},
            {'Q', "/REVERSE"},
            {'E', "/STOP"},
            {'F', "/BOMB"}
        };

        private void txtCommandChar_TextInput_1(object sender, TextCompositionEventArgs e)
        {
            char c = char.ToUpper(e.Text[0]);
            if (commands.ContainsKey(c))
            {
                txtCommand.Text = commands[c];
                Send_Click(this, new RoutedEventArgs());
            }
        }
    }
}
